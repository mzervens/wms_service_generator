<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class WMS_conf_template
{
    static function build($name, $cachePaths)
    {
        $contents = '<?php
/**
 * Starp apaļajām iekavām var ievietot ceļus uz n
 * arcgis ģenerētajiem kešiem.
 *
 * Katru ceļu jānorāda šādā struktūrā: "path/to/cache/"
 * Ceļam ir jābeidzas keša folderī, kas satur šādus failus / folderus: _alllayers, conf.xml, conf.cdi
 *
 * Ceļu norādīšanas paraugs
 *
 * $cachePaths = array
 *  (
 *   
 *	"/data/cache_wgs_15/WGS_world_v15/wms_1801",
 *	"/data/cache_wgs_15/WGS_Baltic_v15/wms_1801"
 *   
 *  );
 *
 *
 *
 */
$cachePaths = array
(
';


        for($i = 0; $i < count($cachePaths); $i++)
        {
            if($i != 0)
            {
                $contents .= ",". PHP_EOL;
            }
            
            $contents .= "\t" . '"'.$cachePaths[$i].'"';
        }
        
        $contents .= PHP_EOL . ');';
        
        return file_put_contents(__DIR__ . '/' . $name . '_conf.php', $contents);

    }
    
    
}
