<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class WMS_file_template
{
    static function build($name, $projectionCode, $engineOptions)
    {
        $contents = '<?php

//error_reporting(0);

set_time_limit(20*60);
define("BROWSER_CACHE_DURATION", 31*24*60*60);

require_once "../server_scripts/functions_log.php";
require_once "../server_scripts/connect_db.php";

require_once "get_users_real_ip.php";
require_once "GetCapabilities.php";
require_once "GetStyles.php";
require_once "GetSchemaExtension.php";
require_once "GetLegend.php";
require_once "handleRequestParameters.php";
require_once \'imageFactory.php\';
require_once \''.$name.'_conf.php\';
require_once \'ParseEnvelope.php\';
require_once \'UnsupportedRequest.php\';
require_once \'ValidateBoundLimits.php\';

$DATABASE_OFFLINE = false;

$con = new Connection(DB_TIPS_KIJS);
if($con->connection == false)
{
    $DATABASE_OFFLINE = true;
}


$client_id = -1;
$intAllowedRequestType = 0;
$wmsVerArr = array(1, 1, 1);
';
        
        $contents .= '$projection = "'.$projectionCode.'";';
        
        $contents .= '

$arr = explode(\',\', $requestBBOX);

if($isZXY == true || $requestBBOX == "")
{
    $arr[0] = $arr[1] = $arr[2] = $arr[3] = 0;
}

if($wms_request_version == "")
{
    $wms_request_version = WMSVersion::V_1_0_0;
}

$wmsVerArr = WMSVersionStrToArr($wms_request_version);

if ($con->connection)
{
    $kijs = GetKijsOrDieTryin($con, "images/no_images/", $arr[0], $arr[1], $arr[2], $arr[3], $client_id, $intAllowedRequestType);
}


if ($isZXY == false && $requestWidth > 0 && $requestHeight > 0)
{
    $intTileCount = ceil(($requestWidth * $requestHeight) / (256 * 256));
}
else
{
    $intTileCount = 1;
}

$restrictedBounds = Array();

if(!$DATABASE_OFFLINE)
{   
    $ret = LogMapRequest($kijs, $client_id, $intTileCount);

    $con->Execute("SELECT MinX, MinY, MaxX, MaxY FROM clients WHERE clients.Kijs = \'" . $kijs ."\'");

    $restrictedBounds = Array();
    while($row = mysql_fetch_array($con->result))
    {
        array_push($restrictedBounds, $row);
    }

    $con->Close();

    $tmp = $restrictedBounds[0][\'MinX\'];
    $restrictedBounds[0][\'MinX\'] = $restrictedBounds[0][\'MinY\'];
    $restrictedBounds[0][\'MinY\'] = $tmp;

    $tmp = $restrictedBounds[0][\'MaxX\'];
    $restrictedBounds[0][\'MaxX\'] = $restrictedBounds[0][\'MaxY\'];
    $restrictedBounds[0][\'MaxY\'] = $tmp;



    if ($ret == false) die("bad log");
}

';

        if($projectionCode == 'EPSG:3059')
        {
            $contents .= '

if ($request == "GetCapabilities")
{
   // $envelope = parseEnvelope($cachePaths[0] . \'/conf.cdi\');
    $envelope = new stdClass();
    $envelope->xMin = -441316;
    $envelope->xMax = 1940650;
    $envelope->yMin = -710055;
    $envelope->yMax = 1233364;
    $wms_link = \'http://\' . $serverName . \'/\' . $kijs . \'/\' . $wmsProjection . \'/\' . $wmsVersion . \'/\';
    $host = \'http://\' . $serverName . \'/\';
    $minScaleDenom = 47247.023810;
    printCapabilities($host, $wms_link, $projection, $envelope, $minScaleDenom, $wmsVerArr);
    exit();

}    
';
        }
        else
        {
            $contents .= '
if ($request == "GetCapabilities")
{
    $envelope = parseEnvelope($cachePaths[0] . \'/conf.cdi\');
    $wms_link = \'http://\' . $serverName . \'/\' . $kijs . \'/\' . $wmsProjection . \'/\' . $wmsVersion . \'/?\';
    $host = \'http://\' . $serverName . \'/\';
        $minScaleDenom = 47247.023810;
    printCapabilities($host, $wms_link, $projection, $envelope, $minScaleDenom);
    exit();

}';
        }
        
        
        $contents .= '
else if ($request == \'GetLegend\')
{
    printLegend();
    exit();
}
else if ($request == \'GetStyles\')
{
    printStyles();
    exit();
}
else if ($request == \'GetSchemaExtension\')
{
    printSchemaExtension();
    exit();
}
// The request is not supported
else if($request != \'GetMap\' && $isZXY != true)
{
    printUnsupportedRequest(256, 256);
    exit();
}

else if($request == \'GetMap\' && ($requestWidth <= 0 || $requestWidth == "" || $requestHeight <= 0 || $requestHeight == ""))
{
    printUnsupportedRequest(256, 256);
    exit();

}

if(!empty($restrictedBounds))
{
    if(isset($restrictedBounds[0][\'MinX\']) && isset($restrictedBounds[0][\'MinY\']) && isset($restrictedBounds[0][\'MaxX\']) && isset($restrictedBounds[0][\'MaxY\'])
        && $restrictedBounds[0][\'MinX\'] != NULL && $restrictedBounds[0][\'MinY\'] != NULL && $restrictedBounds[0][\'MaxX\'] != NULL && $restrictedBounds[0][\'MaxY\'] != NULL)
    {
        $valBounds = new stdClass();
        $valLimits = new stdClass();

        $valLimits->minX = $restrictedBounds[0][\'MinX\'];
        $valLimits->minY = $restrictedBounds[0][\'MinY\'];
        $valLimits->maxX = $restrictedBounds[0][\'MaxX\'];
        $valLimits->maxY = $restrictedBounds[0][\'MaxY\'];

        $valBounds->minX = $arr[0];
        $valBounds->minY = $arr[1];
        $valBounds->maxX = $arr[2];
        $valBounds->maxY = $arr[3];

        $validator = new validateBoundLimits($valBounds, $valLimits, $projection, $wms_request_version);

        if(!$validator->validate())
        {
            die(\'Request is outside limited bounding box. 
                Valid requests are within these bounds: \' .
                $valLimits->minX . \' \' . $valLimits->minY . \' \' . $valLimits->maxX . \' \' . $valLimits->maxY);
        }
    }
}
';


    $returnMisImg = "false";
    $returnMisImgZXY = "";
    if(isset($engineOptions['returnMissingImage']) && $engineOptions['returnMissingImage'])
    {
        $returnMisImg = "true";
        $returnMisImgZXY = ', $ifMissingTileRetImg = true';
    }

$contents .= 'header(\'Access-Control-Allow-Origin: *\');

if($isZXY == true)
{
    $imageFactory = new imageFactory(null, null, null, null, null, null, $responseFormatZXY, $kijs, $zZXY, $xZXY, $yZXY, $cachePaths'.$returnMisImgZXY.');
    return;
}
';
        
        
        
        
        
        $contents .= '

$imageFactory = new imageFactory($arr[0], $arr[1], $arr[2], $arr[3], $requestWidth, $requestHeight, $responseFormat, $kijs, null, null, null, $cachePaths, $returnMissImage = '.$returnMisImg.', $wms_request_version);


return;';
        
        return file_put_contents(__DIR__ . '/' . $name . '.php', $contents);
    }
    
    
}
