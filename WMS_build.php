<?php
error_reporting(E_ALL);
ini_set('display_errors', 1);

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
require_once __DIR__ . '/WMS_conf_template.php';
require_once __DIR__ . '/WMS_file_template.php';
require_once __DIR__ . '/WMS_service_route_template.php';
require_once __DIR__ . '/WMS_service_xyz_route_template.php';


$services = json_decode(file_get_contents(__DIR__ . '/WMS_services.json'), true);
if(isset($services['services']))
{
    $services = $services['services'];
}
else
{
    $services = array();
}

for($i = 0; $i < count($services); $i++)
{
    $service = $services[$i];
    $engineOpts = array();
    
    if(isset($service['engineOptions']))
    {
        $engineOpts = $service['engineOptions'];
    }
    
    if($service['type'] == 'WMS')
    {
        WMS_file_template::build($service['fileName'], $service['projection'], $engineOpts);
        WMS_conf_template::build($service['fileName'], $service['esriCachePaths']);
    }
    
    
}


Service_Route_Template::build($services);
Service_XYZ_Route_Template::build($services);

echo "<pre>";
print_r($services);
echo "</pre>";
 
