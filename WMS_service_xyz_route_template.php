<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
class Service_XYZ_Route_Template
{
    static function build($services)
    {
        
        $servicesOrdered = array();
        
        for($i = 0; $i < count($services); $i++)
        {
            $serv = $services[$i];
            if($serv['type'] != 'WMS')
            {
                continue;
            }
            
            if(!isset($serv['includeInXYZ']) || $serv['includeInXYZ'] == false)
            {
                continue;
            }
            
            if(isset($servicesOrdered[$serv['category']]))
            {
                $servicesOrdered[$serv['category']][] = $serv; 
            }
            else
            {
                $servicesOrdered[$serv['category']] = array();
                $servicesOrdered[$serv['category']][] = $serv; 
            }
        }
        
        
        $contents = '';
        
        $contents .= '<?php

if (isset($_GET[\'kijs\'],$_GET[\'name\'],$_GET[\'v\'],$_GET[\'x\'],$_GET[\'y\'],$_GET[\'z\']))
{
    
    include(\'../server_scripts/functions_log.php\');
    include(\'../server_scripts/connect_db.php\');
    $con = new Connection(DB_TIPS_KIJS);

    $client_id = -1;
    $intAllowedRequestType = 0;
    // tikai, ja ir konekcija uz db
    if ($con->connection)
    {
        $kijs = GetKijsOrDieTryin($con, \'images/no_images/\', 0, 0, 0, 0, $client_id, $intAllowedRequestType);
    }
    
    $ret = LogMapRequest($kijs, $client_id, 1);
    if ($ret == false) die(\'bad log\');
    
    switch (trim($_GET[\'name\']))
    {
        case \'wgs\':
                if ($_GET[\'v\'] == \'13\')
                {
                    $ext = \'png\';
                    $cache_dir = \'/data/cache/wms_wgs_v13\';
                    
                    $intOffsetY = 0;
                    if (intval($_GET[\'z\'])>=10)
                    {
                        $intOffsetY = -1;
                    }
                    
                    $strPath = $cache_dir . \'/\' . intval($_GET[\'z\']) . \'/\' . intval($_GET[\'x\']) . \'/\' . (intval($_GET[\'y\'])+$intOffsetY) . \'.\' . $ext;

                    if (file_exists($strPath))
                    {
                        header(\'Content-Type: image/\' . $ext);
                        echo file_get_contents($strPath);
                        exit();   
                    }
                }
		if ($_GET[\'v\'] == \'14\')
                {
                    $ext = \'png\';
                    $cache_dir = \'/data/cache/wgs_v14\';
                    
                    $intOffsetY = 0;
                    if (intval($_GET[\'z\'])>=10)
                    {
                        $intOffsetY = -1;
                    }
                    
                    $strPath = $cache_dir . \'/\' . intval($_GET[\'z\']) . \'/\' . intval($_GET[\'x\']) . \'/\' . (intval($_GET[\'y\'])) . \'.\' . $ext;

                    if (file_exists($strPath))
                    {
                        header(\'Content-Type: image/\' . $ext);
                        echo file_get_contents($strPath);
                        exit();   
                    }
                }';
        
    if(isset($servicesOrdered['wgs']))
    {
        $wgsServ = $servicesOrdered['wgs'];
        for($i = 0; $i < count($wgsServ); $i++)
        {
            $s = $wgsServ[$i];
            
            $contents .= '                if ($_GET[\'v\'] == \'' . $s['version'] . '\')
                {
                    include(\''.$s['fileName'].'.php\');
                    exit();
                }                
';
        }
    }


    $contents .= '
                break;';

				
	
    foreach($servicesOrdered as $key => $vals)
    {
        if($key == 'wgs')
        {
           continue; 
        }
        
        $contents .= '
        case \''.$key.'\':
';
        
        for($i = 0; $i < count($vals); $i++)
        {
            $s = $vals[$i];
            
            $contents .= '                if ($_GET[\'v\'] == \'' . $s['version'] . '\')
                {
                    include(\''.$s['fileName'].'.php\');
                    exit();
                }                
';
        }
        
        $contents .= '
                break;';
        
    }
    
        
    $contents .= '
	}
    
}

header("HTTP/1.0 404 Not Found");

?>';
        
     return file_put_contents(__DIR__ . '/get_tile_zxy.php', $contents);   
        
    }
}
