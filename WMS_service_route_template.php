<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Service_Route_Template
{
    
    static function build($services)
    {
        
        $servicesOrdered = array();
        
        for($i = 0; $i < count($services); $i++)
        {
            $serv = $services[$i];
            if($serv['type'] != 'WMS')
            {
                continue;
            }
            
            if(isset($servicesOrdered[$serv['category']]))
            {
                $servicesOrdered[$serv['category']][] = $serv; 
            }
            else
            {
                $servicesOrdered[$serv['category']] = array();
                $servicesOrdered[$serv['category']][] = $serv; 
            }
        }
        
        
        
        
        $contents = "";
        $contents .= '<?php

require_once "get_users_real_ip.php";

if (isset($_GET[\'srv\']))
{
    echo 20;
    exit();
}


    if (isset($_GET[\'dbg\']))
    {
        //print_r($_GET);
        //exit();
    }
chdir(\'/home/maps/public_html/kijs/wms/\');


if (isset($_GET[\'kijs\'],$_GET[\'name\'],$_GET[\'v\']))
{
    switch (trim($_GET[\'name\']))
    {
    
        case \'wgs\':
                if ($_GET[\'v\'] == \'11\')
                {
                    include(\'wgs_v11.php\');
                }
                else if ($_GET[\'v\'] == \'13\')
                {
                    include(\'wgs_v13.php\');
                }
                else if ($_GET[\'v\'] == \'WMTS\')
                {
                    include(\'WMTS.php\');
                }

';
        
    if(isset($servicesOrdered['wgs']))
    {
        $wgsServ = $servicesOrdered['wgs'];
        for($i = 0; $i < count($wgsServ); $i++)
        {
            $s = $wgsServ[$i];
            
            $contents .= '                else if ($_GET[\'v\'] == \'' . $s['version'] . '\')
                {
                    include(\''.$s['fileName'].'.php\');
                }                
';
        }
    }
    
    $contents .= '
                break;';
    
    
    
    $contents .= '
        case \'lks\':
                if ($_GET[\'v\'] == \'11\')
                {
                    include(\'lks_v11.php\');
                }
                else if ($_GET[\'v\'] == \'12\')
                {
                    include(\'lks_v12.php\');
                }
                else if ($_GET[\'v\'] == \'13\')
                {
                    include(\'lks_v13.php\');
                }
                else if ($_GET[\'v\'] == \'WMTS\')
                {
                    include(\'WMTS_LKS.php\');
                }
                else if ($_GET[\'v\'] == \'LKS_102440\')
                {
                    include(\'LKS_102440.php\');
                }
                else if ($_GET[\'v\'] == \'LGIA_ORTO\')
                {
                    include(\'LKS_lgia_ortofoto_service.php\');
                }
';
    
    if(isset($servicesOrdered['lks']))
    {
        $lksServ = $servicesOrdered['lks'];
        for($i = 0; $i < count($lksServ); $i++)
        {
            $s = $lksServ[$i];
            
            $contents .= '                else if ($_GET[\'v\'] == \'' . $s['version'] . '\')
                {
                    include(\''.$s['fileName'].'.php\');
                }                
';
        }
    }
    
    $contents .= '
                break;';
    
    foreach($servicesOrdered as $key => $vals)
    {
        if($key == 'wgs' || $key == 'lks')
        {
           continue; 
        }
        
        $contents .= '
        case \''.$key.'\':';
        
        for($i = 0; $i < count($vals); $i++)
        {
            $s = $vals[$i];
            $useElse = 'else';
            if($i == 0)
            {
                $useElse = '';
            }
            
            $contents .= '                '.$useElse.' if ($_GET[\'v\'] == \'' . $s['version'] . '\')
                {
                    include(\''.$s['fileName'].'.php\');
                }                
';
        }
        
        $contents .= '
                break;';
        
    }
    
        
    $contents .= '
	}
    
}

?>';
        
    return file_put_contents(__DIR__ . '/get_wms.php', $contents);    
        
    }
}