# README #


### What is this repository for? ###

* Program generates WMS(and other service) files. If new service needs to be created then there usually needs to be added two php scripts that define the service then this service needs to be linked in two other php scripts that publish it. This project generates the needed files from a JSON file which contains all necessary service descriptions.


### How do I get set up? ###

* Services are defined in the WMS_services.json as JSON objects with properties that should be looked in other project scripts
* Files are generates by running the WMS_build.php file


### Who do I talk to? ###

something

* Matiss Zervens